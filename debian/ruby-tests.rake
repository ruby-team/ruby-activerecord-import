load 'Rakefile'

sh 'cp test/database.yml.sample test/database.yml'

ENV['AR_VERSION'] = '6.1'

task :default => ['test:sqlite3', 'test:spatialite']
